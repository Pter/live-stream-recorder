import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import axios from 'axios';
import * as xml2js from 'xml2js';
import { Timeout } from '@nestjs/schedule';
import * as fs from 'fs';

import { Stream } from './stream.schema';
import { StreamsRecorderService } from 'src/streams-recorder/streams-recorder.service';
import { VideoChannel } from 'src/video-channels/video-channels.schema';

@Injectable()
export class StreamsService {
  private acknowledgedStreams: string[] = [];

  constructor(
    @InjectModel('Stream') private streamModel: Model<Stream>,
    private streamsRecorderService: StreamsRecorderService,
  ) {}


  public async getStream(id: string){ 
    return this.streamModel.findOne({id});
  }

  public async getAllOngoingStreams(){
    const eligibleStreams = await this.streamModel.find({
      recordingStatus: 'inProgress',
    });
    return eligibleStreams;
  }

  public async getNonProcessedFinishedStreams(){
    const eligibleStreams = await this.streamModel.find({
      recordingStatus: 'finished',
      mergingStatus: {$ne: 'finished'}
    });
    return eligibleStreams;
  }

  // Start this on boot
  @Timeout(1000)
  private async startInProgressStreams() {
    const eligibleStreams = await this.streamModel.find({
      recordingStatus: 'inProgress',
    });

    console.log(
      `Found ${eligibleStreams.length} streams that needs to be continued`,
    );

    for (const stream of eligibleStreams) {
      this.streamsRecorderService.acceptRecordingRequest(stream.toObject());
    }
  }

  public async acceptOngoingStreamNotification(
    videoChannel: VideoChannel,
    streamNotification: {
      id: string;
    },
  ) {
    // First of all we need to check this stream in the database
    const dbStream = await this.streamModel.findOne({
      id: streamNotification.id,
    });

    // console.log('received stream object:', dbStream);

    if (dbStream) {
      // we already know that stream, nice
      this.streamsRecorderService.acceptRecordingRequest(dbStream.toObject());
    } else {
      //this stream is a new for us, so we should create an object for it
      const newDbStream = new this.streamModel({
        id: streamNotification.id,
        videoChannelId: videoChannel._id,
      });

      const videoPageHtml = (
        await axios.get(`${videoChannel.url}/live`, {
          responseType: 'text',
          headers: {
            // cookie: process.env.YOUTUBE_COOKIE,
          },
        })
      ).data;

      fs.writeFile("videoPageHtml.txt", videoPageHtml, function (err) {
        if (err) return console.log(err);
        console.log("videoPageHtml > videoPageHtml.txt");
      });

      const objectRegexp = /"dashManifestUrl\\":\\"(.*)\\",\\"hls/gi;

      let dashManifestUrl = objectRegexp.exec(videoPageHtml)[1];

      console.log('received dash manifest', dashManifestUrl);

      // dashManifestUrl = decodeURIComponent(dashManifestUrl);
      dashManifestUrl = dashManifestUrl.replace(/\\\//g, '/');

      console.log('dash manifest:', dashManifestUrl);

      newDbStream.dashManifestUrl = dashManifestUrl;

      const dashManifestText = (
        await axios.get(dashManifestUrl, {
          responseType: 'text',
        })
      ).data;

      const dashManifest = await xml2js.parseStringPromise(dashManifestText);

      // newDbStream.dashManifest = JSON.stringify(dashManifest);

      // console.log('dashManifest', dashManifest);
      const Period = dashManifest.MPD.Period[0];

      const sequenceLength = dashManifest.MPD['$'].minimumUpdatePeriod.match(
        /PT(.).*/,
      )[1];
      newDbStream.sequenceLength = sequenceLength;

      // console.log('period', Period);
      const adaptationSetArray = Period.AdaptationSet;
      // console.log('adaptationSetArray', adaptationSetArray);

      let audioStreamBaseUrl;
      let videoStreamBaseUrl;
      let segmentsList;

      let videoFormat;

      for (const adaptaionSet of adaptationSetArray) {
        console.log('________________________');
        // console.log('adaptaionSet:', adaptaionSet);
        if (adaptaionSet['$'].mimeType === 'audio/mp4') {
          const representation = adaptaionSet.Representation.pop();
          // console.log('selected representation:', representation);
          audioStreamBaseUrl = representation.BaseURL[0];
          segmentsList = representation.SegmentList[0].SegmentURL;
        }
        if (
          // adaptaionSet['$'].mimeType === 'video/webm' ||
          adaptaionSet['$'].mimeType === 'video/mp4'
        ) {
          const representation = adaptaionSet.Representation.pop();
          console.log('selected representation:', representation);
          videoStreamBaseUrl = representation.BaseURL[0];
          videoFormat = adaptaionSet['$'].mimeType.split('/').pop();
        }
      }
      console.log(
        'received urls to streams',
        audioStreamBaseUrl,
        videoStreamBaseUrl,
      );
      console.log('segments:', segmentsList.length);

      console.log('last segment:', segmentsList[segmentsList.length - 1]);

      const firstSegment = 0;

      newDbStream.startingSequenceId = firstSegment;
      newDbStream.currentSequenceId = firstSegment;

      newDbStream.audioStreamBaseUrl = audioStreamBaseUrl;
      newDbStream.videoStreamBaseUrl = videoStreamBaseUrl;

      newDbStream.videoFormat = videoFormat;

      console.log('newdbstream:', newDbStream._id, newDbStream.id);

      await newDbStream.save();

      this.streamsRecorderService.acceptRecordingRequest(
        newDbStream.toObject(),
      );
    }
  }

  public async updateStream(
    streamId: string,
    streamUpdate: { currentSequenceId?: number; recordingStatus?: string, mergingStatus?: string },
  ): Promise<Stream> {
    const stream = await this.streamModel.updateOne(
      { id: streamId },
      { $set: streamUpdate },
    );
    return stream;
  }
}

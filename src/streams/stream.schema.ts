import * as mongoose from 'mongoose';

export const StreamSchema = new mongoose.Schema({
  id: { type: String, unique: true },
  videoChannelId: { type: mongoose.SchemaTypes.ObjectId},

  dashManifestUrl: String,
  currentSequenceId: Number,
  startingSequenceId: Number,
  audioStreamBaseUrl: String,
  videoStreamBaseUrl: String,
  videoFormat: String,
  recordingStatus: String,
  mergingStatus: String,
  // dashManifest: { type: String},
  sequenceLength: { type: Number },
});

export interface Stream extends mongoose.Document {
  id: string;
  videoChannelId: mongoose.Types.ObjectId;

  dashManifestUrl: string;
  currentSequenceId: number;
  startingSequenceId: number;
  audioStreamBaseUrl: string;
  videoStreamBaseUrl: string;
  videoFormat: string;
  recordingStatus: string;
  mergingStatus: string;
  // dashManifest: string;
  sequenceLength: number;
}

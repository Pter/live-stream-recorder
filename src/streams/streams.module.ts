import { Module, forwardRef } from '@nestjs/common';
import { StreamsService } from './streams.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StreamSchema } from './stream.schema';
import { StreamsRecorderModule } from 'src/streams-recorder/streams-recorder.module';
import { StreamsController } from './streams.controller';
import { MergedStreamSequencesModule } from 'src/merged-stream-sequences/merged-stream-sequences.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Stream', schema: StreamSchema }]),
    StreamsRecorderModule,
    // MergedStreamSequencesModule,
    forwardRef(() => MergedStreamSequencesModule)
  ],
  providers: [StreamsService],
  exports: [StreamsService],
  controllers: [StreamsController],
})
export class StreamsModule {}

import { Controller, Param, Get, Redirect, Header } from '@nestjs/common';
import { MergedStreamSequencesService } from 'src/merged-stream-sequences/merged-stream-sequences.service';
import { StreamsService } from './streams.service';
import { pd } from 'pretty-data';

@Controller('streams')
export class StreamsController {
  constructor(
    private mergedStreamSequencesService: MergedStreamSequencesService,
    private streamsService: StreamsService,
  ) {}

  @Get('/getPlaylist/:streamId')
  async getPlaylistForStream(@Param('streamId') streamId: string) {
    const stream = await this.streamsService.getStream(streamId);

    // const mergedSequences = await this.mergedStreamSequencesService.getMergedSequencesForStream(
    //   stream._id,
    // );

    const playlistString = `#EXTM3U
#EXT-X-VERSION:3
#EXT-X-STREAM-INF:BANDWIDTH=540863
https://e9ea57c69e23.eu.ngrok.io/streams/getPlaylist/${stream.id}/play.m3u8
`;
    return playlistString;
  }

  @Get('/getPlaylist/:streamId/play.mpd')
  @Header('Content-Type', 'application/xml')
  async getPlaylistMpd(@Param('streamId') streamId: string) {
    const stream = await this.streamsService.getStream(streamId);

    const mergedSequences = await this.mergedStreamSequencesService.getMergedSequencesForStream(
      stream._id,
    );

    const result = `<?xml version="1.0" encoding="UTF-8"?>
<MPD 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xmlns="urn:mpeg:dash:schema:mpd:2011" 
  xmlns:scte35="http://www.scte.org/schemas/35/2014SCTE35.xsd" 
  xsi:schemaLocation="urn:mpeg:dash:schema:mpd:2011 DASH-MPD.xsd" 
  profiles="urn:mpeg:dash:profile:isoff-main:2011" 
  type="static" 
  minBufferTime="PT5.000S" 
  maxSegmentDuration="PT100.005S" 
  mediaPresentationDuration="PT5000.680S"
>
      <Period id="0">
        <AdaptationSet id="0" contentType="video" frameRate="60/1" maxWidth="1920" maxHeight="1080" par="16:9" lang="und">
          <Role schemeIdUri="urn:mpeg:DASH:role:2011" value="main"/>
          <Representation id="0" codecs="avc1.640028" mimeType="video/mp4" bandwidth="21209932" width="1920" height="1080" sar="1:1">
            <BaseURL>https://msk-storage-test.fox.faith/api/files/getFile/</BaseURL>
            <SegmentList duration="100">
              ${mergedSequences
                .map(x => `<SegmentURL media="${x.storage[0].data.fileId}"/>`)
                .join('\r\n')}
            </SegmentList>
          </Representation>
        </AdaptationSet>
      </Period>
      </MPD>`;

    return pd.xmlmin(result);
  }

  // @Get('/getPlaylist/:streamId/:seqId')
  // @Redirect('https://ef74f4c61adb.eu.ngrok.io/api/files/getFile/5ee647085acd66250cbc914a', 301)
  // async getMpdSequence(){
  //   return 'redirect...'
  // }

  @Get('/getPlaylist/:streamId/play.m3u8')
  async getPlayPlaylistForStream(@Param('streamId') streamId: string) {
    const stream = await this.streamsService.getStream(streamId);

    const mergedSequences = await this.mergedStreamSequencesService.getMergedSequencesForStream(
      stream._id,
    );

    const maxTsLength = Math.ceil(
      Math.max(...mergedSequences.map(x => x.mediaInfo.streams[0].duration)),
    );

    let playlistString = '#EXTM3U\r\n';
    playlistString += '\r\n';
    playlistString += '#EXT-X-ALLOW-CACHE:YES\r\n';
    playlistString += '#EXT-X-PLAYLIST-TYPE:VOD\r\n';
    playlistString += '#EXT-X-VERSION:3\r\n';
    playlistString += '#EXT-X-MEDIA-SEQUENCE:0\r\n';
    playlistString += `#EXT-X-TARGETDURATION:${maxTsLength}\r\n`;

    // #EXTINF:100.0
    // https://msk-storage-test.fox.faith/api/files/getFile/5ee647085acd66250cbc914a
    // #EXTINF:100.0
    // https://msk-storage-test.fox.faith/api/files/getFile/5ee647085acd66250cbc914a
    // #EXTINF:100.0
    // https://msk-storage-test.fox.faith/api/files/getFile/5ee647085acd66250cbc914a

    for (const seq of mergedSequences) {
      playlistString += `#EXTINF:${seq.mediaInfo.streams[0].duration},\r\n`;
      playlistString += `https://master.fcloud.fox.faith/api/files/getFile/${seq.storage[0].data.fileId}\r\n`;
    }

    playlistString += `#EXT-X-ENDLIST\r\n`;
    return playlistString;
  }

  @Get('/test')
  test() {
    return `#EXTM3U
    #EXT-X-VERSION:3
    #EXT-X-MEDIA-SEQUENCE:0
    #EXT-X-ALLOW-CACHE:YES
    #EXT-X-TARGETDURATION:11
    #EXTINF:100.031022,
    https://msk-storage-test.fox.faith/api/files/getFile/5ee655935acd66250cbc9170
    #EXTINF:10.028811,
    https://s2.content.video.llnw.net/smedia/42f4e71183054396907c0dea18241568/yd/yLpoUEilVrxhhYSkpsyGMnn9t0N3AYxWmoMsM7Faw/francstireurs_entrevue_ep472_seq24.mpegts/playlist1.ts
    #EXTINF:10.026600,
    https://s2.content.video.llnw.net/smedia/42f4e71183054396907c0dea18241568/yd/yLpoUEilVrxhhYSkpsyGMnn9t0N3AYxWmoMsM7Faw/francstireurs_entrevue_ep472_seq24.mpegts/playlist2.ts
    #EXTINF:9.977956,
    https://s2.content.video.llnw.net/smedia/42f4e71183054396907c0dea18241568/yd/yLpoUEilVrxhhYSkpsyGMnn9t0N3AYxWmoMsM7Faw/francstireurs_entrevue_ep472_seq24.mpegts/playlist3.ts
    #EXTINF:10.032322,
    https://s2.content.video.llnw.net/smedia/42f4e71183054396907c0dea18241568/yd/yLpoUEilVrxhhYSkpsyGMnn9t0N3AYxWmoMsM7Faw/francstireurs_entrevue_ep472_seq24.mpegts/playlist4.ts
    #EXTINF:10.030111,
    https://s2.content.video.llnw.net/smedia/42f4e71183054396907c0dea18241568/yd/yLpoUEilVrxhhYSkpsyGMnn9t0N3AYxWmoMsM7Faw/francstireurs_entrevue_ep472_seq24.mpegts/playlist5.ts
    #EXTINF:9.981478,
    https://s2.content.video.llnw.net/smedia/42f4e71183054396907c0dea18241568/yd/yLpoUEilVrxhhYSkpsyGMnn9t0N3AYxWmoMsM7Faw/francstireurs_entrevue_ep472_seq24.mpegts/playlist6.ts
    #EXTINF:10.012633,
    https://s2.content.video.llnw.net/smedia/42f4e71183054396907c0dea18241568/yd/yLpoUEilVrxhhYSkpsyGMnn9t0N3AYxWmoMsM7Faw/francstireurs_entrevue_ep472_seq24.mpegts/playlist7.ts
    #EXTINF:10.010422,
    playlist8.ts
    #EXTINF:10.031433,
    playlist9.ts
    #EXTINF:9.982778,
    playlist10.ts
    #EXTINF:10.013933,
    playlist11.ts
    #EXTINF:10.011722,
    playlist12.ts
    #EXTINF:9.986300,
    playlist13.ts
    #EXTINF:10.017456,
    playlist14.ts
    #EXTINF:10.015244,
    playlist15.ts
    #EXTINF:9.989822,
    playlist16.ts
    #EXTINF:10.020978,
    playlist17.ts
    #EXTINF:10.018767,
    playlist18.ts
    #EXTINF:10.016556,
    playlist19.ts
    #EXTINF:9.991122,
    playlist20.ts
    #EXTINF:10.022278,
    playlist21.ts
    #EXTINF:10.020067,
    playlist22.ts
    #EXTINF:9.994644,
    playlist23.ts
    #EXTINF:10.025800,
    playlist24.ts
    #EXTINF:10.023589,
    playlist25.ts
    #EXTINF:9.998167,
    playlist26.ts
    #EXTINF:10.029322,
    playlist27.ts
    #EXTINF:10.027111,
    playlist28.ts
    #EXTINF:9.978456,
    playlist29.ts
    #EXTINF:10.032833,
    playlist30.ts
    #EXTINF:10.030622,
    playlist31.ts
    #EXTINF:10.028411,
    playlist32.ts
    #EXTINF:9.979767,
    playlist33.ts
    #EXTINF:10.010922,
    playlist34.ts
    #EXTINF:10.031933,
    playlist35.ts
    #EXTINF:9.983289,
    playlist36.ts
    #EXTINF:10.014444,
    playlist37.ts
    #EXTINF:10.012233,
    playlist38.ts
    #EXTINF:10.010022,
    playlist39.ts
    #EXTINF:9.984600,
    playlist40.ts
    #EXTINF:10.015756,
    playlist41.ts
    #EXTINF:10.013544,
    playlist42.ts
    #EXTINF:9.988111,
    playlist43.ts
    #EXTINF:10.019267,
    playlist44.ts
    #EXTINF:10.017056,
    playlist45.ts
    #EXTINF:9.991633,
    playlist46.ts
    #EXTINF:10.022789,
    playlist47.ts
    #EXTINF:10.020578,
    playlist48.ts
    #EXTINF:10.018367,
    playlist49.ts
    #EXTINF:9.992944,
    playlist50.ts
    #EXTINF:10.024100,
    playlist51.ts
    #EXTINF:10.021889,
    playlist52.ts
    #EXTINF:9.996456,
    playlist53.ts
    #EXTINF:10.027611,
    playlist54.ts
    #EXTINF:10.025400,
    playlist55.ts
    #EXTINF:9.976756,
    playlist56.ts
    #EXTINF:10.031133,
    playlist57.ts
    #EXTINF:10.028922,
    playlist58.ts
    #EXTINF:9.980278,
    playlist59.ts
    #EXTINF:10.011433,
    playlist60.ts
    #EXTINF:10.032444,
    playlist61.ts
    #EXTINF:10.030233,
    playlist62.ts
    #EXTINF:9.981578,
    playlist63.ts
    #EXTINF:10.012733,
    playlist64.ts
    #EXTINF:10.010522,
    playlist65.ts
    #EXTINF:9.985100,
    playlist66.ts
    #EXTINF:10.016256,
    playlist67.ts
    #EXTINF:10.014056,
    playlist68.ts
    #EXTINF:9.988622,
    playlist69.ts
    #EXTINF:10.019778,
    playlist70.ts
    #EXTINF:10.017567,
    playlist71.ts
    #EXTINF:10.015356,
    playlist72.ts
    #EXTINF:9.989922,
    playlist73.ts
    #EXTINF:10.021078,
    playlist74.ts
    #EXTINF:10.018867,
    playlist75.ts
    #EXTINF:9.993444,
    playlist76.ts
    #EXTINF:10.024600,
    playlist77.ts
    #EXTINF:10.022400,
    playlist78.ts
    #EXTINF:10.020189,
    playlist79.ts
    #EXTINF:9.994756,
    playlist80.ts
    #EXTINF:10.025911,
    playlist81.ts
    #EXTINF:10.023700,
    playlist82.ts
    #EXTINF:9.998267,
    playlist83.ts
    #EXTINF:10.029422,
    playlist84.ts
    #EXTINF:10.027211,
    playlist85.ts
    #EXTINF:9.978578,
    playlist86.ts
    #EXTINF:10.032956,
    playlist87.ts
    #EXTINF:10.030744,
    playlist88.ts
    #EXTINF:10.028533,
    playlist89.ts
    #EXTINF:9.979878,
    playlist90.ts
    #EXTINF:10.011033,
    playlist91.ts
    #EXTINF:10.032044,
    playlist92.ts
    #EXTINF:9.983389,
    playlist93.ts
    #EXTINF:10.014544,
    playlist94.ts
    #EXTINF:10.012333,
    playlist95.ts
    #EXTINF:9.986922,
    playlist96.ts
    #EXTINF:10.018078,
    playlist97.ts
    #EXTINF:10.015867,
    playlist98.ts
    #EXTINF:10.013656,
    playlist99.ts
    #EXTINF:9.988222,
    playlist100.ts
    #EXTINF:10.019378,
    playlist101.ts
    #EXTINF:10.017167,
    playlist102.ts
    #EXTINF:9.991733,
    playlist103.ts
    #EXTINF:8.313322,
    playlist104.ts
    #EXT-X-ENDLIST
    `;
  }
}

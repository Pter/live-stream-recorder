import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MergedStreamSequence } from './merged-stream-sequence.schema';
import { Model, Types, Promise } from 'mongoose';
import { Timeout } from '@nestjs/schedule';
import { promises as fs } from 'fs';
import * as usualFs from 'fs';
import * as FormData from 'form-data';

import * as os from 'os';
import * as path from 'path';
import * as ffmpeg from 'fluent-ffmpeg';
import * as ffmpegPath from 'ffmpeg-static';
import {path as ffprobePath} from 'ffprobe-static';
import * as Minio from 'minio';
import axios from 'axios';

import { StreamsService } from '../streams/streams.service';
import { StreamSequencesService } from 'src/stream-sequences/stream-sequences.service';
import { CreateMergedStreamSequenceDto } from './dto/create-merged-stream-sequence.dto';
import { Stream } from 'src/streams/stream.schema';

const SEQUENCES_PER_MERGE = 50;

@Injectable()
export class MergedStreamSequencesService {
  private minioClient = new Minio.Client({
    endPoint: process.env.MINIO_ENDPOINT,
    port: parseInt(process.env.MINIO_PORT, 10) || 9000,
    useSSL: false,
    accessKey: process.env.MINIO_ACCESS_KEY,
    secretKey: process.env.MINIO_SECRET_KEY,
  });

  constructor(
    @InjectModel('MergedStreamSequence')
    private mergedStreamSequenceModel: Model<MergedStreamSequence>,
    private streamsService: StreamsService,
    private streamSequencesService: StreamSequencesService,
  ) {
    ffmpeg.setFfmpegPath(ffmpegPath);
    ffmpeg.setFfprobePath(ffprobePath);
  }

  async getMergedSequencesForStream(
    streamObjectId: Types.ObjectId,
  ): Promise<MergedStreamSequence[]> {
    return this.mergedStreamSequenceModel.find({ streamId: streamObjectId });
  }

  private createSequenceLink({ videoId, sequenceNumber }) {
    return `http://${process.env.MINIO_ENDPOINT}:${process.env.MINIO_PORT}/test-bucket/youtube-videos/${videoId}/${sequenceNumber}.mp4`;
  }

  private async createMergedSequence(mrg: CreateMergedStreamSequenceDto) {
    const newMergedSequence = new this.mergedStreamSequenceModel(mrg);
    return newMergedSequence.save();
  }

  // private async prepareMergeSequence(){

  // }

  @Timeout(10000)
  private async sequenceMerger() {
    console.log('sequenceMerger started');
    while (true) {
      console.log('sequenceMerger step');
      const allOngoingStreams = await this.streamsService.getAllOngoingStreams();

      console.log(
        'ongoing streams:',
        allOngoingStreams.map(x => x.id),
      );

      for (const stream of allOngoingStreams) {
        console.log('working with stream:', stream.id);

        const existingMergedSequences = await this.getMergedSequencesForStream(
          stream._id,
        );
        console.log('existingMergedSequences', existingMergedSequences);

        const mergedSequencesCount = existingMergedSequences.reduce(
          (acc, v) => (acc += v.sequencesCount),
          0,
        );

        console.log('mergedSequencesCount', mergedSequencesCount);

        const overallSequencesCount = await this.streamSequencesService.getMaxSequenceNumberForStream(
          stream._id,
        );
        console.log('getMaxSequenceNumberForStream', overallSequencesCount);

        if (
          overallSequencesCount - mergedSequencesCount >=
          SEQUENCES_PER_MERGE
        ) {
          await this.mergeStream(stream, existingMergedSequences);
        }
      }

      const eligibleFinishedStreams = await this.streamsService.getNonProcessedFinishedStreams();

      for (const stream of eligibleFinishedStreams) {
        console.log('look at eligible finished stream:', stream.id);
        const existingMergedSequences = await this.getMergedSequencesForStream(
          stream._id,
        );
        console.log('existingMergedSequences', existingMergedSequences);

        const mergedSequencesCount = existingMergedSequences.reduce(
          (acc, v) => (acc += v.sequencesCount),
          0,
        );

        console.log('mergedSequencesCount', mergedSequencesCount);

        const overallSequencesCount = await this.streamSequencesService.getMaxSequenceNumberForStream(
          stream._id,
        );
        console.log('getMaxSequenceNumberForStream', overallSequencesCount);

        const mergeableSequencesCount =
          overallSequencesCount - mergedSequencesCount ;

        console.log('mergeableSequencesCount', mergeableSequencesCount);

        if (mergeableSequencesCount > 0) {
          await this.mergeStream(
            stream,
            existingMergedSequences,
            mergeableSequencesCount,
          );
          this.streamsService.updateStream(stream.id, {
            mergingStatus: 'finished',
          });
        }
      }

      await new Promise(resolve => setTimeout(resolve, 8000));
    }
  }

  private async mergeStream(
    stream: Stream,
    existingMergedSequences: MergedStreamSequence[],
    seqenceToMerge?: number,
  ) {
    const mergedSequencesCount = seqenceToMerge || SEQUENCES_PER_MERGE;

    let filesStr = '';

    const maxProcessedSequenceId =
      existingMergedSequences.length > 0
        ? Math.max(...existingMergedSequences.map(x => x.lastSequence))
        : 0;

    console.log('maxProcessedSequenceId', maxProcessedSequenceId);

    await fs.rmdir(path.join(os.tmpdir(), stream.id), { recursive: true });
    await fs.mkdir(path.join(os.tmpdir(), stream.id), {
      recursive: true,
    });

    const firstSequenceId =
      maxProcessedSequenceId == 0 ? 0 : maxProcessedSequenceId + 1;
    const lastSequenceId = firstSequenceId + mergedSequencesCount - 1;

    const tasksArray = [];
    for (let i = firstSequenceId; i <= lastSequenceId; i += 1) {
      // filesStr += `file '${this.createSequenceLink({
      //   videoId: stream.id,
      //   sequenceNumber: i,
      // })}'\r\n`;
      const task = {
        fileName: `${i}.mp4`,
        fileLocalPath: path.join(os.tmpdir(), stream.id, `${i}.mp4`),
      };
      filesStr += `file '${task.fileLocalPath}'\r\n`;
      tasksArray.push(task);
    }

    // download all files to folder
    const promisesStorage = [];
    for (const task of tasksArray) {
      promisesStorage.push(
        this.minioClient.fGetObject(
          'test-bucket',
          'youtube-videos/' + stream.id + '/' + task.fileName,
          task.fileLocalPath,
        ),
      );
    }

    await Promise.all(promisesStorage);

    console.log('ALL FILES DOWNLOADED!');

    const tmpTxtFilename = `video_list_${stream.id}_${firstSequenceId}.txt`;
    const tmpTxtFilePath = path.join(os.tmpdir(), tmpTxtFilename);

    await fs.writeFile(tmpTxtFilePath, filesStr);

    const resultFilePath = path.join(
      os.tmpdir(),
      `merge_result_${stream.id}_${firstSequenceId}.ts`,
    );

    let tsOffset = 0;
    tsOffset = existingMergedSequences.map(x => x.mediaInfo.streams[0].duration).reduce((a, b) => a + b, 0)

    console.log('calculated ts offset:', tsOffset);

    const ffPromise = await new Promise((resolve, reject) => {
      ffmpeg()
        .input(tmpTxtFilePath)
        .inputOptions([
          '-f concat',
          '-safe 0',
          '-protocol_whitelist file,tcp,http,https',
        ])
        .on('start', function(commandLine) {
          console.log('Spawned Ffmpeg with command: ' + commandLine);
        })
        .on('progress', progress => {
          console.log(`[ffmpeg] ${JSON.stringify(progress)}`);
        })
        .on('error', err => {
          console.log(`[ffmpeg] error: ${err.message}`);
          console.log('full error:', err);
          reject(err);
        })
        .on('end', () => {
          console.log('[ffmpeg] finished');
          resolve();
        })
        .outputOptions(['-c copy', '-movflags +faststart', `-output_ts_offset ${tsOffset}`])
        .save(resultFilePath);
    });

    fs.unlink(tmpTxtFilePath);
    await fs.rmdir(path.join(os.tmpdir(), stream.id), { recursive: true });

    const resultFileStats = await fs.stat(resultFilePath);
    const resultFileProbe = await  new Promise ((resolve ,reject) => {
      ffmpeg.ffprobe(resultFilePath, (err, data) => {
        if (err){
          return resolve({})
        }
        return resolve(data);
      })
    });
    //example for copy

    const formData = new FormData();
    formData.append('file', usualFs.createReadStream(resultFilePath));

    const requestConfig = {
      headers: {
        ...formData.getHeaders(),
      },
      json: true,
      maxContentLength: 1024 ** 3,
    };

    console.log('sending request to storage', requestConfig);

    const sendResult = await axios.post(
      `https://master.fcloud.fox.faith/api/files/uploadFile`,
      formData,
      requestConfig,
    );

    console.log('uploading result:', sendResult.data);

    this.createMergedSequence({
      streamId: stream._id,
      videoFormat: 'ts',
      bytes: resultFileStats.size,
      firstSequence: firstSequenceId,
      lastSequence: lastSequenceId,
      mediaInfo: resultFileProbe,
      storage: [
        { name: 'freecloud', data: { fileId: sendResult.data?.newFile?._id } },
      ],
    });

    const pathsForDelete = [];
    for (const task of tasksArray) {
      pathsForDelete.push('youtube-videos/' + stream.id + '/' + task.fileName);
    }

    // const removeObjects = await this.minioClient.removeObjects(
    //   'test-bucket',
    //   pathsForDelete,
    // );
    // console.log('removeobjects results', removeObjects);
  }
}

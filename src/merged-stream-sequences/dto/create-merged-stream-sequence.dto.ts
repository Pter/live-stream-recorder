import * as mongoose from 'mongoose';


export class CreateMergedStreamSequenceDto {
  streamId: mongoose.Types.ObjectId;
  videoFormat: string;
  bytes: number;
  firstSequence: number;
  lastSequence: number;
  storage: [{ name: string; data: any }];
  mediaInfo: any
}
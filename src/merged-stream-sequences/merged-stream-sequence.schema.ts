import * as mongoose from 'mongoose';

export const MergedStreamSequenceSchema = new mongoose.Schema({
  streamId: { type: mongoose.SchemaTypes.ObjectId },
  videoFormat: String,
  bytes: { type: Number },
  firstSequence: { type: Number },
  lastSequence: { type: Number },
  storage: [
    {
      name: { type: String },
      data: { type: mongoose.SchemaTypes.Mixed },
    },
  ],
  mediaInfo: {type: mongoose.SchemaTypes.Mixed}
});

MergedStreamSequenceSchema.virtual('sequencesCount').get(function() {
  return this.lastSequence - this.firstSequence + 1;
});

export interface MergedStreamSequence extends mongoose.Document {
  streamId: mongoose.Types.ObjectId;
  videoFormat: string;
  bytes: number;
  firstSequence: number;
  lastSequence: number;
  storage: [{ name: string; data: any }];
  sequencesCount: number;
  mediaInfo: any;
}

import { Module, forwardRef } from '@nestjs/common';
import { MergedStreamSequencesService } from './merged-stream-sequences.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MergedStreamSequenceSchema } from './merged-stream-sequence.schema';
import { StreamsModule } from 'src/streams/streams.module';
import { StreamSequencesModule } from 'src/stream-sequences/stream-sequences.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'MergedStreamSequence', schema: MergedStreamSequenceSchema },
    ]),
    // StreamsModule,
    forwardRef(() => StreamsModule),
    StreamSequencesModule
  ],
  providers: [MergedStreamSequencesService],
  exports: [MergedStreamSequencesService]
})
export class MergedStreamSequencesModule {}

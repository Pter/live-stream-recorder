import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { NestEmitterModule } from 'nest-emitter';
import { EventEmitter } from 'events';

import { StreamsModule } from './streams/streams.module';
import { StreamsRecorderModule } from './streams-recorder/streams-recorder.module';
import { VideoChannelsModule } from './video-channels/video-channels.module';
import { StreamSequencesModule } from './stream-sequences/stream-sequences.module';
import { MergedStreamSequencesModule } from './merged-stream-sequences/merged-stream-sequences.module';


@Module({
  imports: [
    ConfigModule.forRoot(),
    VideoChannelsModule,
    NestEmitterModule.forRoot(new EventEmitter()),
    ScheduleModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }),
    StreamsModule,
    StreamsRecorderModule,
    StreamSequencesModule,
    MergedStreamSequencesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

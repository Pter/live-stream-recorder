import { Module, forwardRef } from '@nestjs/common';
import { StreamsRecorderService } from './streams-recorder.service';
import { StreamsModule } from 'src/streams/streams.module';
import { StreamSequencesModule } from 'src/stream-sequences/stream-sequences.module';

@Module({
  // imports: [StreamsModule],
  imports: [forwardRef(() => StreamsModule), StreamSequencesModule],
  providers: [StreamsRecorderService],
  exports: [StreamsRecorderService]
})
export class StreamsRecorderModule {}

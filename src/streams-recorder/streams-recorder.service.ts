import { Injectable, Inject, forwardRef } from '@nestjs/common';
import * as events from 'events';
import axios from 'axios';
import * as Minio from 'minio';
import * as util from 'util';
import { promises as fs } from 'fs';
import * as os from 'os';
import * as path from 'path';
import * as ffmpeg from 'fluent-ffmpeg';

import * as ffmpegPath from 'ffmpeg-static';

import { Stream } from 'src/streams/stream.schema';
import { StreamsService } from 'src/streams/streams.service';
import { StreamSequencesService } from 'src/stream-sequences/stream-sequences.service';
import { resolve } from 'dns';


@Injectable()
export class StreamsRecorderService {
  private minioClient = new Minio.Client({
    endPoint: process.env.MINIO_ENDPOINT,
    port: parseInt(process.env.MINIO_PORT, 10) || 9000,
    useSSL: false,
    accessKey: process.env.MINIO_ACCESS_KEY,
    secretKey: process.env.MINIO_SECRET_KEY,
  });

  private eventEmitter = new events.EventEmitter();

  private activeStreamRecordings = [];

  constructor(
    @Inject(forwardRef(() => StreamsService))
    private streamsService: StreamsService,
    private readonly streamSequencesService: StreamSequencesService,
  ) {
    ffmpeg.setFfmpegPath(ffmpegPath);

    this.eventEmitter.on(
      'startStreamRecording',
      this.newStreamRecordingHandler.bind(this),
    );
  }

  public acceptRecordingRequest(recordingRequest: any) {
    const existingRecording = this.activeStreamRecordings.find(
      x => x.id === recordingRequest.id,
    );
    if (existingRecording) {
      console.log('already recording');
    } else {
      this.eventEmitter.emit('startStreamRecording', recordingRequest);
    }
  }

  private async newStreamRecordingHandler(stream: Stream): Promise<void> {
    this.activeStreamRecordings.push(stream);

    let currentSequenceId = stream.currentSequenceId;
    let lastSuccessfulSequenceTime = new Date();
    let continousErrorsCount = 0;

    while (true) {
      try {
        console.log('current sequence id:', currentSequenceId);

        const sequenceInfo = await this.downloadSequence({
          videoUrl: `${stream.videoStreamBaseUrl}sq/${currentSequenceId}`,
          audioUrl: `${stream.audioStreamBaseUrl}sq/${currentSequenceId}`,
          folderName: stream.id,
          fileName: `${currentSequenceId}`,
        });

        // console.log('sequenceInfo:', sequenceInfo);

        await this.streamsService.updateStream(stream.id, {
          currentSequenceId: currentSequenceId,
          recordingStatus: 'inProgress',
        });

        await this.streamSequencesService.createStreamSequence({
          sequenceId: currentSequenceId,
          videoFormat: stream.videoFormat,
          streamId: stream._id,
          bytes: sequenceInfo.fileStats.size,
        });

        currentSequenceId++;

        continousErrorsCount = 0;
        lastSuccessfulSequenceTime = new Date();
      } catch (err) {
        console.log(
          'received error while downoading sequence:',
          util.inspect(err, { depth: 1 }),
        );

        continousErrorsCount++;

        if (
          continousErrorsCount > 50 &&
          new Date().getTime() - lastSuccessfulSequenceTime.getTime() >
            10 * 60 * 1000 // if stream is inactive 10 minutes
        ) {
          await this.streamsService.updateStream(stream.id, {
            currentSequenceId: currentSequenceId,
            recordingStatus: 'finished',
          });
          break;
        }

        await new Promise(resolve => setTimeout(resolve, 5000));
      }
    }
  }

  private async downloadSequence({ videoUrl, audioUrl, folderName, fileName }) {
    const tmpAudioFilePath = path.join(
      os.tmpdir(),
      `${folderName}_${fileName}.m4a`,
    );
    const tmpVideoFilePath = path.join(
      os.tmpdir(),
      `${folderName}_${fileName}.mp4`,
    );

    const tmpResultVideoFilePath = path.join(
      os.tmpdir(),
      `${folderName}_${fileName}_result.mp4`,
    );

    const downloadFileHelper = async (fileUrl, filePath) => {
      const audioResponse = await axios({
        method: 'get',
        url: fileUrl,
        responseType: 'arraybuffer',
      });

      await fs.writeFile(filePath, audioResponse.data);
    };

    const audioPromise = downloadFileHelper(audioUrl, tmpAudioFilePath);

    const videoPromise = downloadFileHelper(videoUrl, tmpVideoFilePath);

    await Promise.all([audioPromise, videoPromise]);

    console.log('tmpVideoFilePath', tmpVideoFilePath);
    console.log('tmpAudioFilePath', tmpAudioFilePath);

    const ffPromise = await new Promise((resolve, reject) => {
      ffmpeg()
        .input(tmpVideoFilePath)
        .format('mp4')
        .input(tmpAudioFilePath)
        .format('m4a')
        .audioCodec('copy')
        .videoCodec('copy')
        .on('start', function(commandLine) {
          console.log('Spawned Ffmpeg with command: ' + commandLine);
        })
        .on('progress', progress => {
          console.log(`[ffmpeg] ${JSON.stringify(progress)}`);
        })
        .on('error', err => {
          console.log(`[ffmpeg] error: ${err.message}`);
          console.log('full error:', err);
          reject(err);
        })
        .on('end', () => {
          console.log('[ffmpeg] finished');
          resolve();
        })
        .format('mp4')
        .save(tmpResultVideoFilePath);
    });

    fs.unlink(tmpAudioFilePath);
    fs.unlink(tmpVideoFilePath);

    const uploadResult = await this.minioClient.fPutObject(
      'test-bucket',
      'youtube-videos/' + folderName + '/' + fileName + '.mp4',
      tmpResultVideoFilePath,
      {},
    );

    const stats = await fs.stat(tmpResultVideoFilePath);

    fs.unlink(tmpResultVideoFilePath);


    return { uploadId: uploadResult, fileStats: stats};
  }

  private async downloadFile(url, { folderName, fileName }) {
    const response = await axios({
      url,
      method: 'GET',
      responseType: 'stream',
      validateStatus: function(status) {
        return status === 200; // default
      },
    });

    console.log('response:', response.status);

    const uploadResult = await new Promise((resolve, reject) =>
      this.minioClient.putObject(
        'test-bucket',
        'youtube-videos/' + folderName + '/' + fileName,
        response.data,
        response.headers['content-length'],
        {},
        function(err, etag) {
          if (err) {
            reject(err);
            return console.log(err);
          }
          console.log('File uploaded successfully.');
          resolve(response);
        },
      ),
    );
    console.log('Downloaded and uploaded');
  }

  private async downloadFileStream(url, { folderName, fileName }) {
    const response = await axios({
      url,
      method: 'GET',
      responseType: 'stream',
      validateStatus: function(status) {
        return status === 200; // default
      },
    });

    console.log('response:', response.status);

    return response.data;
  }
}

import * as mongoose from 'mongoose';


export class CreateStreamSequenceDto {
  readonly sequenceId: number;
  readonly streamId: mongoose.Types.ObjectId;
  readonly videoFormat: string;
  readonly bytes: number;
}
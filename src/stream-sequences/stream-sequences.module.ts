import { Module } from '@nestjs/common';
import { StreamSequencesService } from './stream-sequences.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StreamSequenceSchema } from './stream-sequence.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'StreamSequence', schema: StreamSequenceSchema },
    ]),
  ],
  providers: [StreamSequencesService],
  exports: [StreamSequencesService],
})
export class StreamSequencesModule {}

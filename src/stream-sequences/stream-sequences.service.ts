import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { StreamSequence } from './stream-sequence.schema';
import { CreateStreamSequenceDto } from './dto/create-stream-sequence.dto';
import { Timeout } from '@nestjs/schedule';

@Injectable()
export class StreamSequencesService {
  constructor(
    @InjectModel('StreamSequence') private streamSequenceModel: Model<StreamSequence>,
  ) {}

  async createStreamSequence(createStreamSequenceDto: CreateStreamSequenceDto): Promise<StreamSequence>{
    const streamSequence = new this.streamSequenceModel(createStreamSequenceDto);
    await streamSequence.save();
    return streamSequence;
  }

  public async getSequencesCountForStream(streamObjectId: Types.ObjectId): Promise<number> {
    return this.streamSequenceModel.countDocuments({streamId: streamObjectId});
  }

  public async getMaxSequenceNumberForStream(streamObjectId: Types.ObjectId): Promise<number>{
    const doc = await  this.streamSequenceModel.findOne({streamId: streamObjectId}).sort({sequenceId:-1})
    return doc.sequenceId;
  }

}

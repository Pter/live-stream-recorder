import * as mongoose from 'mongoose';

export const StreamSequenceSchema = new mongoose.Schema({
  sequenceId: { type: Number },
  streamId: { type: mongoose.SchemaTypes.ObjectId },
  videoFormat: String,
  bytes: {type: Number},
}).index({sequenceId: 1, streamId:1}, {unique: true}).index({streamId: 1});

export interface StreamSequence extends mongoose.Document {
  sequenceId: number;
  streamId: mongoose.Types.ObjectId;
  videoFormat: string;
  bytes: number;
}

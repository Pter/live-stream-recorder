import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { VideoChannel } from './video-channels.schema';
import { CreateVideoChannelDto } from './dto/create-video-channel.dto';
import { Timeout } from '@nestjs/schedule';
import axios from 'axios';
import * as fs from 'fs';
import { StreamsService } from 'src/streams/streams.service';

@Injectable()
export class VideoChannelsService {
  constructor(
    @InjectModel('VideoChannel') private videoChannelModel: Model<VideoChannel>,
    private readonly streamsService: StreamsService,
  ) {}

  async create(
    createVideoChannelDto: CreateVideoChannelDto,
  ): Promise<VideoChannel> {
    const createdVideoChannel = new this.videoChannelModel(
      createVideoChannelDto,
    );
    return createdVideoChannel.save();
  }

  async getAll(): Promise<VideoChannel[]> {
    return this.videoChannelModel.find().exec();
  }

  @Timeout(5000)
  async checkChannelsStatus() {
    console.log('checkChannelsStatus executed');
    while (true) {
      const channels = await this.getAll();

      for (const channel of channels) {
        const result = await this.getChannelStatus(channel);
        console.log(`${channel.url}:`, result);

        if (result.isStreamActive) {
          this.streamsService.acceptOngoingStreamNotification(channel, {
            id: result.streamInfo.id,
          });
        }

        await new Promise(resolve => setTimeout(resolve, 5000));
      }

      await new Promise(resolve => setTimeout(resolve, 10000));
    }
  }

  async getChannelStatus(
    channel,
  ): Promise<{ isStreamActive: boolean; streamInfo?: { id: string } }> {
    const resultObj: any = {};

    const pageHtml = (
      await axios.get(`${channel.url}/live`, {
        responseType: 'text',
        headers: {
          cookie:
            process.env.YOUTUBE_COOKIE,
        },
      })
    ).data;

    const isStreamActive = pageHtml.includes('dashManifestUrl') ? true : false;

    if (isStreamActive) {
      resultObj.streamInfo = {};
      const videoExtractRegexp = /\\"videoId\\":\\"([a-zA-Z0-9_-]*)\\"/;

      fs.writeFile('pageHtml.txt', pageHtml, function(err) {
        if (err) return console.log(err);
        console.log('pageHtml > pageHtml.txt');
      });

      const regexResult = videoExtractRegexp.exec(pageHtml);
      if (regexResult && regexResult[1]) {
        const videoId = videoExtractRegexp.exec(pageHtml)[1];
        resultObj.streamInfo.id = videoId;
      } else {
        throw new Error('temporaly erroring here');
      }
    }

    resultObj.isStreamActive = isStreamActive;

    return resultObj;
  }
}

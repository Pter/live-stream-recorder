import { Controller, Get, Post, Body } from '@nestjs/common';
import { VideoChannelsService } from './video-channels.service';
import { CreateVideoChannelDto } from './dto/create-video-channel.dto';

@Controller('video-channel')
export class VideoChannelsController {
  constructor(private readonly videoChannelsService: VideoChannelsService) {}

  @Get()
  getAllVideoChannels() {
    return this.videoChannelsService.getAll();
  }

  @Post()
  createVideoChannel(@Body() createVideoChannelDto: CreateVideoChannelDto) {
    return this.videoChannelsService.create(createVideoChannelDto);
  }
}

import * as mongoose from 'mongoose';

export const VideoChannelSchema = new mongoose.Schema({
  url: { type: String, unique: true },
});

export interface VideoChannel extends mongoose.Document {
  url: string;
}

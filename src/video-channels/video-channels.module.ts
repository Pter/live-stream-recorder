import { Module } from '@nestjs/common';
import { VideoChannelsController } from './video-channels.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { VideoChannelSchema } from './video-channels.schema';
import { VideoChannelsService } from './video-channels.service';
import { StreamsModule } from 'src/streams/streams.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'VideoChannel', schema: VideoChannelSchema },
    ]),
    StreamsModule
  ],
  controllers: [VideoChannelsController],
  providers: [VideoChannelsService],
})
export class VideoChannelsModule {}
